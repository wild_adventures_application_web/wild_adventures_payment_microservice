package com.wildadventures.microservice.payment.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wildadventures.microservice.payment.domain.Order;

@FeignClient("order-microservice")
@RequestMapping("${order-microservice.context-path}")
public interface OrderClient {

	@GetMapping(path = "/orders/{id}")
	Order getOrder(@PathVariable("id") Long id);
	
	@PutMapping(path = "/orders")
	Order updateMany(@RequestBody List<Order> orders);

}
