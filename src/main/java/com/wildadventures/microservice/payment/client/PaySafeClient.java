package com.wildadventures.microservice.payment.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wildadventures.microservice.payment.domain.Payment;

import feign.Headers;

@FeignClient("pay-safe")
@RequestMapping(path = "${pay-safe.context-path}", headers = {
		"Content-Type=application/json",
		"Authorization=Basic ${pay-safe.basic-auth-base64}" 
	})
public interface PaySafeClient {

	@PostMapping(path = "/accounts/${pay-safe.account-id}/auths")
	Payment authorizationAndSettlement(@RequestBody Payment payment);
}
