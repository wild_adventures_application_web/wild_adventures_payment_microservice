package com.wildadventures.microservice.payment.service.impl;



import java.sql.Timestamp;
import java.time.Instant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wildadventures.microservice.payment.client.OrderClient;
import com.wildadventures.microservice.payment.client.PaySafeClient;
import com.wildadventures.microservice.payment.domain.Order;
import com.wildadventures.microservice.payment.domain.Payment;
import com.wildadventures.microservice.payment.domain.PaymentRequest;
import com.wildadventures.microservice.payment.service.EntityService;

@Service
public class PaymentServiceImpl implements EntityService<PaymentRequest, Payment> {

	@Autowired
	private OrderClient orderClient;
	
	@Autowired
	private PaySafeClient paySafeClient;
	
	Logger logger = LoggerFactory.getLogger(PaymentServiceImpl.class);

	@Override
	public Payment executePayment(PaymentRequest paymentRequest) {
		Payment payment = paymentRequest.getPayment();
		
		Timestamp date = new Timestamp(Instant.now().toEpochMilli());
		StringBuilder merchantRefNum = new StringBuilder("P");
		merchantRefNum.append(date.getTime());
		
		int arraySize = paymentRequest.getOrders().size();
		for(int i = 0; i < arraySize; i ++) {
			Order order = this.orderClient.getOrder(paymentRequest.getOrders().get(i).getId());
			order.setPaymentDate(date);
			
			merchantRefNum.append("-");
			merchantRefNum.append(order.getId());
			
			Integer orderPrice = (int) (order.getPrice() * 100);
			payment.setAmount(payment.getAmount() + orderPrice);
			
			paymentRequest.getOrders().set(i, order);
		}
		
		payment.setSettleWithAuth(true);
		payment.setMerchantRefNum(merchantRefNum.toString());
		Payment paymentDone = this.paySafeClient.authorizationAndSettlement(payment);
		
		this.orderClient.updateMany(paymentRequest.getOrders());
		
		return paymentDone;
	}
}
