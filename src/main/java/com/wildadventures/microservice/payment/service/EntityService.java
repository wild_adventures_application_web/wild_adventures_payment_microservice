package com.wildadventures.microservice.payment.service;

import java.util.List;


public interface EntityService<T, U> {

	U executePayment(T entities);
	
}
