package com.wildadventures.microservice.payment.domain;

import java.util.List;

public class Settlement {

	public List<Link> links = null;
	public String id;
	public String merchantRefNum;
	public String txnTime;
	public String status;
	public Integer amount;
	public Integer availableToRefund;

}