package com.wildadventures.microservice.payment.domain;

import java.util.List;

import javax.validation.Valid;

import lombok.Data;

@Data
public class PaymentRequest {
	
	@Valid
	private Payment payment;
	@Valid
	private List<Order> orders;

}
