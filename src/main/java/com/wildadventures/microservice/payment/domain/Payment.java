
package com.wildadventures.microservice.payment.domain;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class Payment {

	public String id;
    public String merchantRefNum;
    public Integer amount = 0;
    public Boolean settleWithAuth;
    public Boolean dupCheck;
    @Valid
    @NotNull
    public Card card;
    public Profile profile;
    @Valid
    @NotNull
    public BillingDetails billingDetails;
    public String customerIp;
    public String description;
    public String txnTime;
    public String status;
    public Integer availableToSettle;
    public String authCode;
    public String currencyCode;
    public String avsResponse;
    public String cvvVerification;
    public List<Settlement> settlements = null;
    public List<Link> links = null;

}
