package com.wildadventures.microservice.payment.domain;

import java.sql.Timestamp;

import lombok.Data;

@Data
public class Order {

	private Long id;
	private Long adventureEventId;
	private Long userId;
	private Timestamp orderDate;
	private Timestamp paymentDate;
	private Double price;

}
