
package com.wildadventures.microservice.payment.domain;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class BillingDetails {

	@NotNull
    public String street;
	@NotNull
	public String city;
	@NotNull
	public String state;
	@NotNull
	public String country;
	@NotNull
	public String zip;

}
