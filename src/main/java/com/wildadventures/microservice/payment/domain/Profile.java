
package com.wildadventures.microservice.payment.domain;

import lombok.Data;

@Data
public class Profile {

    public String firstName;
    public String lastName;
    public String email;

}
