
package com.wildadventures.microservice.payment.domain;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class CardExpiry {

	@NotNull
	@Min(1)
	@Max(12)
	public Integer month;
	@NotNull
	@Min(2019)
	@Max(2999)
	public Integer year;

}
