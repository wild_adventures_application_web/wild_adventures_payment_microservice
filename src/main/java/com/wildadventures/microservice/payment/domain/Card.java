
package com.wildadventures.microservice.payment.domain;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class Card {

	@NotNull
	@Size(min = 8, max = 20)
	public String cardNum;
	@Valid
	@NotNull
	public CardExpiry cardExpiry;
	public Integer cvv;
	public String type;
	public String lastDigits;

}
