package com.wildadventures.microservice.payment.controller.impl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wildadventures.microservice.payment.controller.EntityController;
import com.wildadventures.microservice.payment.domain.Order;
import com.wildadventures.microservice.payment.domain.Payment;
import com.wildadventures.microservice.payment.domain.PaymentRequest;
import com.wildadventures.microservice.payment.service.EntityService;

@RestController
@RequestMapping(path = "/payments", produces = "application/json")
public class PaymentController implements EntityController<PaymentRequest, Payment> {

	@Autowired
	private EntityService<PaymentRequest, Payment> paymentService;

	@Override
	@PostMapping
	public ResponseEntity<Payment> post(@Valid @RequestBody PaymentRequest paymentRequest) {
		Payment payment = this.paymentService.executePayment(paymentRequest);
		
		return ResponseEntity.ok(payment);
	}
}
