package com.wildadventures.microservice.payment.controller;

import org.springframework.http.ResponseEntity;

public interface EntityController<T, U> {
	
	ResponseEntity<U> post(T paymentRequest);

}
