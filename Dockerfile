FROM openjdk:8-jre-alpine

ARG appName=app 
ARG version=1.0

LABEL name="${appName}"
LABEL version="${version}"

WORKDIR "/myApp"

COPY "target/${appName}-${version}.jar" "app.jar"

CMD ["java", "-jar", "app.jar"]
